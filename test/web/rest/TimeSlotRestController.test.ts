import { Connection } from 'typeorm';
import request from 'supertest';
import Koa from 'koa';
import koa from '@infrastructure/koa';
import { initContainer } from '@container';
import { TimeSlot } from '@domain/entity/TimeSlot';
import InitDB from '../../support/InitDB';
import CleanDB from '../../support/cleanDB';

describe('TimeSlots API: timeslots', () => {
  let dbConnection: Connection;
  let app: Koa;
  const professionalName = 'uncle bob';
  const slotDate = '2021-09-02';
  const slotStartTime = '08:30';
  const slotEndTime = '09:30';
  const parsedSlotDate = new Date(slotDate);

  beforeAll(async () => {
    dbConnection = await InitDB();
    app = await koa(await initContainer());
  });

  afterAll(async () => {
    await dbConnection.close();
  });

  afterEach(async () => {
    await CleanDB();
  });

  describe('POST /timeslots', () => {
    test.only('Time slot creation', async () => {
      const result = await request(app.callback())
        .post('/timeslots')
        .send({ professionalName, slotDate, slotStartTime, slotEndTime });

      const { status, body } = result;

      expect(status).toBe(200);
      expect(body.status).toBe('success');
      expect(body.data.professionalName).toBe(professionalName);
      expect(body.data.slotDate).toBe(parsedSlotDate.toISOString());
      expect(body.data.slotStartTime).toBe(slotStartTime);
      expect(body.data.slotEndTime).toBe(slotEndTime);
    });

    test.only('Failed Time Slot creation missing start time', async () => {
      const result = await request(app.callback())
        .post('/timeslots')
        .send({ professionalName, parsedSlotDate, slotEndTime });

      const  { status, body } = result;
      expect(status).toBe(400);
      expect(body.status).toBe('error');
    });

    test.only('Failed slot creation - invalid slot date', async () => {
      const { status } = await request(app.callback())
        .post('/timeslotS')
        .send({ professionalName, slotStartTime, slotEndTime });

      expect(status).toBe(400);
    });
  });

  describe('GET /timeslots', () => {
    test.only('Fetch time slots', async () => {
      await new TimeSlot().create(professionalName, parsedSlotDate, slotStartTime, slotEndTime).save();

      const result = await request(app.callback()).get('/timeslots');
      const { status, body } = result;

      expect(status).toBe(200);
      expect(body.status).toBe('success');
      expect(body.data.items[0].professionalName).toBe(professionalName);
    });
  });

  describe('PATCH /timeslots/:id', () => {
    test.only('Sould update the time slotwith client name to book time', async () => {
      const clientNameToBook = 'steve wozniak';

      const timeSlot: TimeSlot = await new TimeSlot().create(
        professionalName, parsedSlotDate, slotStartTime, slotEndTime).save();
      
      const patcher = {
        professionalName, 
        slotDate,
        slotStartTime,
        slotEndTime,
        clientName: clientNameToBook,
      }

      const { id } = timeSlot;
      const { status, body } = await request(app.callback())
        .patch(`/timeslots/${id}`)
        .send(patcher);

      expect(status).toBe(200);
      expect(body.status).toBe('success');
      expect(body.data.clientName).toBe(clientNameToBook);
    });

    test.only('Should fail update missing time slot ', async () => {
      const result = await request(app.callback())
        .patch('/timeslots/1')
        .send({
          professionalName, slotDate, slotStartTime, slotEndTime,
        });

      const { status, body } = result;
      expect(status).toBe(404);
      expect(body.status).toBe('error');
    });

    test.only('Should fail updat timeslot with time out of range', async () => {
      const slotStartTimeOFR = '04:00';
      const timeSlot: TimeSlot = await new TimeSlot()
        .create(professionalName, parsedSlotDate, slotStartTimeOFR, slotEndTime).save();
      const { id } = timeSlot;

      const { status } = await request(app.callback())
        .patch(`/timeslots/${id}`)
        .send({ professionalName, parsedSlotDate, slotStartTime: '06:00', slotEndTime });

      expect(status).toBe(400);
    });
  });

  describe('DELETE /timeslots/:id', () => {
    test.only('Should delete an existing time slot', async () => {
      const timeslot: TimeSlot = await new TimeSlot().create(
        professionalName, parsedSlotDate, slotStartTime, slotEndTime).save();
      const { id } = timeslot;

      const { status } = await request(app.callback()).delete(`/timeslots/${id}`);

      expect(status).toBe(204);
    });

    test.only('Should fail to delete missing time slot.', async () => {
      const { status, body } = await request(app.callback()).delete('/timeslots/1');

      expect(status).toBe(404);
      expect(body.status).toBe('error');
    });
  });
});
