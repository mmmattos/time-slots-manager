import { TimeSlot } from '@domain/entity/TimeSlot';

describe('TimeSlot Entity', () => {
  const professionalName = 'michael jordan';
  const slotDate = '2021-08-15';
  const parsedSlotDate = new Date(slotDate);
  const slotStartTime = '10:00';
  const slotEndTime = '11:00';

  test('TimeSlot creation', async () => {
    const actual = new TimeSlot()
      .create(professionalName, parsedSlotDate, slotStartTime, slotEndTime);

    expect(actual.professionalName).toBe(professionalName);
    expect(actual.slotDate).toBe(parsedSlotDate);
    expect(actual.slotStartTime).toBe(slotStartTime);
    expect(actual.slotEndTime).toBe(slotEndTime);
  });

  test('TimeSlot velidation error on creation', async () => {
    const slotStartTimeOFR = '05:00';
    try {
      const timeslot = new TimeSlot().create(
        professionalName, parsedSlotDate, slotStartTimeOFR, slotEndTime);
      timeslot.validateSlotTimes();
    } catch (error) {
      expect(error.name).toBe('ValidationError');
    }
  });
});
