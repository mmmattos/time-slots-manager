# timeslots

Time Slots Manager


The reationale behind the decisions for this implementation is simple.
One base route for the API:  /timeslots
The TimeSlots table holds all the data for the professional agenda and the booking.

TimeSlot
id
professionalName
slotDate
slotStarTime
slotEndTime
clientName

Slots are managed by a CRUd on /timeslots.
They also get created in batchs when you POST to /timeslots/availability
Yhid rndpoiny should egt a payload with: professionalName, weekday, Start of availability (i.e. 900) and end of Availability (i.e. 1100)  and number of Weeks (this is something I added of my own). 
For the booking the PATCH on /timeslots. would work.  It wont allow updates if the clientName was already set on a previous PATCH.  So no cnflicts.
I could have created a separate /timeslots/booking. But time was short as already explained and with Father's day yesterday.

Used NOde 12.10.0
Used containerized MySQL.
Used TypeORM  for easy abstraction of the storage layer. 
For the API, used Koa  (by the same makers of Express). Could have been Express or HapiJS. For simplicity

There is a GET route to handle timeslots by date range. 

Got good coverage of unit tests (70% my guess) 

But I couldn't test everything.
So it may need some fixes. Should be linted. 


## To run 

```shell
# docker build
docker build . -t api:latest

# docker run
docker-compose up

## Install Dependencies
# Install all package dependencies
npm install


# run tests
npm run test

# run in development mode (nodemon)
npm run start

# run in production mode
npm run prod

# lint
npm run lint
```

## Try It

- API should  be available at  [http://localhost:9000](http://localhost:9000/timeslots)

Other decisions:
No authentication or authorization on the API.


There is much room for improvement.  Always!