export class TimeSlotValidationException {
  constructor() {
    const error = new Error('Failed to Validate TimeSlot!.');
    error.name = 'ValidationError';

    throw error;
  }
}
