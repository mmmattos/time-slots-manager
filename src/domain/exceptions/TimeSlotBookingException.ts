export class TimeSlotBookingException {
  constructor() {
    const error = new Error('Failed to book or update professional agenda.');
    error.name = 'BookingError';
    
    throw error;
  }
}
