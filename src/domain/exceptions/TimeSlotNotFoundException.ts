export class TimeSlotNotFoundException {
  constructor() {
    const error = new Error('No agenda was found for the professional provided');
    error.name = 'NotFoundError';

    throw error;
  }
}
