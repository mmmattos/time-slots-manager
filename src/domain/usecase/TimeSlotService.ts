import { TimeSlot } from '@domain/entity/TimeSlot';
import { Between } from 'typeorm';
import { TimeSlotBookingException, TimeSlotNotFoundException, TimeSlotValidationException } from '../exceptions';

export default class TimeSlotService {
  public createTimeSlot = async (
    professionalName: string,
    slotDate: string,
    slotStartTime: string,
    slotEndTime: string,
  ): Promise<TimeSlot> => {
    let parsedSlotDate: Date;
    try {
      parsedSlotDate = new Date(slotDate);
    } catch (err) {
      throw new TimeSlotValidationException();
    }
    const timeSlot: TimeSlot = new TimeSlot().create(professionalName, parsedSlotDate, slotStartTime, slotEndTime);

    return timeSlot.save();
  };

  public findTimeSlotsByDateRange = async (from: Date, to: Date): Promise<TimeSlot[]> => 
    TimeSlot.find({
      where: {
        slotDate: Between(from.toISOString, to.toISOString),
      },
    });

  public findTimeSlots = async (start: number, count: number): Promise<[TimeSlot[], number]> =>
    TimeSlot.findAndCount({ skip: start, take: count });

  public updateTimeSlot = async (
    id: number,
    professionalName: string,
    slotDate: Date,
    slotStartTime: string,
    slotEndTime: string,
    clientName: string,
  ): Promise<TimeSlot> => {
    const timeSlot: TimeSlot = await TimeSlotService.findTimeSlot(id);
    timeSlot.professionalName = professionalName;
    timeSlot.slotDate = slotDate;
    timeSlot.slotStartTime = slotStartTime;
    timeSlot.slotEndTime = slotEndTime;
    timeSlot.clientName = clientName;

    if (clientName) {
      throw new TimeSlotBookingException();
    }

    timeSlot.validateSlotTimes();

    return timeSlot.save();
  };

  public deleteTimeSlot = async (id: number): Promise<void> => {
    const timeSlot: TimeSlot = await TimeSlotService.findTimeSlot(id);
    await timeSlot.remove();
  };

  private static async findTimeSlot(id: number) {
    const timeSlot: TimeSlot | undefined = await TimeSlot.findOne({ id });

    if (!timeSlot) {
      throw new TimeSlotNotFoundException();
    }

    return timeSlot;
  }
}
