import { BaseEntity, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { TimeSlotValidationException } from '../exceptions';

@Entity('timeslots')
export class TimeSlot extends BaseEntity {
  @PrimaryGeneratedColumn({ unsigned: true })
  id!: number;

  @Column('varchar', { nullable: false })
  professionalName!: string;

  @Column('datetime', { nullable: false })
  slotDate!: Date;

  @Column('char', { length: 5, nullable: false })
  slotStartTime!: string;

  @Column('char', { length: 5, nullable: false })
  slotEndTime!: string;

  @Column('varchar', { nullable: true })
  clientName?: string;

  @Column('boolean', { nullable: false, default: false })
  deleted!: boolean;

  @CreateDateColumn({ type: 'datetime' })
  createdAt!: Date;

  @UpdateDateColumn({ type: 'datetime' })
  updatedAt!: Date;

  @Column({ type: 'datetime' })
  deletedAt!: Date;

  public create(
    professionalName: string,
    slotDate: Date,
    slotStartTime: string,
    slotEndTime: string,
    clientName?: string | undefined,
  ): TimeSlot {
    const timeSlot: TimeSlot = new TimeSlot();
    timeSlot.professionalName = professionalName;
    timeSlot.slotDate = slotDate;
    timeSlot.slotStartTime = slotStartTime;
    timeSlot.slotEndTime = slotEndTime;
    timeSlot.clientName = clientName;

    return timeSlot;
  }

  public validateSlotTimes(): void {
    const validSlotStartTimeRange = ['08:00', '08:30', '09:00', '09:30', '10:00', '10:30'];
    const index = validSlotStartTimeRange.indexOf(this.slotStartTime);
    if (index < 0) {
      throw new TimeSlotValidationException();
    }
  }
}
