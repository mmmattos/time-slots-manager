import { Context } from 'koa';
import { route, POST, GET, PATCH, DELETE } from 'awilix-koa';
import TimeSlotService from '@domain/usecase/TimeSlotService';
import { TimeSlot } from '@domain/entity/TimeSlot';
import util from 'util';

@route('/timeslots')
export default class TimeSlotRestController {
  private readonly timeSlotService: TimeSlotService;

  constructor(timeSlotService: TimeSlotService) {
    this.timeSlotService = timeSlotService;
  }

  private nextDate = ((dayIndex, nbrWeeks) => {
    const today = new Date();
    const result: Date[] = [];
    for (let i: number = 0; i < nbrWeeks; i += 1) {
      today.setDate(today.getDate() + (dayIndex - 1 - today.getDay() + 7) % 7 + 1);
      today.setHours(0);
      today.setMinutes(0);
      today.setSeconds(0);
      result.push(today);
    }
    return result;
  });

  private formatPeriods = ((t: number) =>
    `00${t.toString()}`.substr(-4)
      .replace(/^(\d{2})(\d{2}).*/, '$1:$2')
  );

  private buildTimeSlots = async (p) => {
    const {
      professionalName, slotDate, slotStartTime, slotEndTime,
    } = p;

    return new Promise((resolve) => resolve(
      this.timeSlotService.createTimeSlot(
        professionalName,
        slotDate,
        slotStartTime,
        slotEndTime,
      )));
  };

  @POST()
  @route('/availability')
  public createTimeSlotsFromAvailability = async (ctx: Context): Promise<Context> => {
    try {
      const {
        professionalName,
        weekday,
        startsAvailability,
        endsAvailability,
        nbrWeeks,
      }: {
        professionalName: string;
        weekday: number;
        startsAvailability: number;
        endsAvailability: number;
        nbrWeeks: number;
      } = ctx.request.body;

      const slotDates = this.nextDate(weekday, nbrWeeks);

      const tsArray: any[] = [];
      for (let d:number = 0; d < slotDates.length; d += 1) {
        for (let t:number = startsAvailability; t <= endsAvailability - 100; t += 100) {
          const delta = ((t % 100 === 0) ? 30 : 70);
          const slotDate = slotDates[d].toISOString().substr(10);
          const slotStartTime = this.formatPeriods(t);
          const slotEndTime = this.formatPeriods(t + delta);
          const tsObj = { professionalName, slotDate, slotStartTime, slotEndTime };
          tsArray.push(tsObj);
          t += delta;
        }  
      }

      const builds = tsArray.map(this.buildTimeSlots);

      const results = await Promise.all(builds);

      return ctx.success({ ...results });


    } catch (error) {
      console.error(error);
      throw error;
    }
  };

  @POST()
  public createTimeSlot = async (ctx: Context): Promise<Context> => {
    try {
      await this.validateTimeSlot(ctx);

      const {
        professionalName,
        slotDate,
        slotStartTime,
        slotEndTime,
      }: {
        professionalName: string;
        slotDate: string;
        slotStartTime: string;
        slotEndTime: string;
      } = ctx.request.body;

      const timeSlot: TimeSlot = await this.timeSlotService.createTimeSlot(
        professionalName,
        slotDate,
        slotStartTime,
        slotEndTime,
      );

      return ctx.success({ ...timeSlot });
    } catch (error) {
      if (error.name === 'ValidationError') {
        return ctx.throw(400, error.message);
      }

      throw error;
    }
  };

  @GET()
  @route('/available')
  public findTimeSlotsByPeriod = async (ctx: Context): Promise<Context> => {
    const defaultTo: Date = new Date();
    const defaultFrom: Date = new Date();

    let { to, from }: any = ctx.request.query;
    const days: number = 7;

    if (!from || !to) {
      to = defaultTo;
      from = defaultFrom.setDate(defaultFrom.getDate() - days);
    }

    const items: TimeSlot[] = await this.timeSlotService.findTimeSlotsByDateRange(from, to);

    return ctx.success({ items, from, to });
  };

  @GET()
  public findTimeSlots = async (ctx: Context): Promise<Context> => {
    const { start = 0, count = 10 }: any = ctx.request.query;

    const [items, total]: [TimeSlot[], number] = await this.timeSlotService.findTimeSlots(start, count);

    return ctx.success({ items, start, count, total });
  };

  @PATCH()
  @route('/:id')
  public updateTimeSlot = async (ctx: Context): Promise<Context> => {
    try {
      await this.validateTimeSlot(ctx);
      await this.validateTimeSlotId(ctx);

      const {
        professionalName,
        slotDate,
        slotStartTime,
        slotEndTime,
        clientName,
      }: {
        professionalName: string;
        slotDate: Date;
        slotStartTime: string;
        slotEndTime: string;
        clientName: string;
      } = ctx.request.body;
      const { id } = ctx.params;

      const timeSlot: TimeSlot = await this.timeSlotService.updateTimeSlot(
        id,
        professionalName,
        slotDate,
        slotStartTime,
        slotEndTime,
        clientName,
      );

      return ctx.success({ ...timeSlot });
    } catch (error) {
      if (error.name === 'ValidationError') {
        return ctx.throw(400, error.message);
      }

      if (error.name === 'NotFoundError') {
        return ctx.throw(404, error.message);
      }

      if (error.name === 'BookingError') {
        return ctx.throw(409, error.message);
      }

      throw error;
    }
  };

  @DELETE()
  @route('/:id')
  public deleteTimeSlot = async (ctx: Context): Promise<Context> => {
    try {
      await this.validateTimeSlotId(ctx);

      const { id } = ctx.params;

      await this.timeSlotService.deleteTimeSlot(id);

      ctx.status = 204;
      return ctx;
    } catch (error) {
      if (error.name === 'NotFoundError') {
        return ctx.throw(404, error.message);
      }

      throw error;
    }
  };

  private validateTimeSlot = async (ctx): Promise<void> => {
    ctx.checkBody('professionalName', 'Invalid professionalName').notEmpty();
    ctx.checkBody('slotDate', 'Invalid slot date').notEmpty();
    ctx.checkBody('slotStartTime', 'Invalid start time').notEmpty();
    ctx.checkBody('slotEndTime', 'Invalid end time').notEmpty();

    const errors = await ctx.validationErrors();

    if (errors) {
      ctx.throw(400, `There have been validation errors: ${util.inspect(errors)}`);
    }
  };

  private validateTimeSlotId = async (ctx): Promise<void> => {
    ctx.checkParams('id').notEmpty().isInt();

    const errors = await ctx.validationErrors();

    if (errors) {
      ctx.throw(400, `There have been validation errors: ${util.inspect(errors)}`);
    }
  };
}
